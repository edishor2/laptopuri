from django.apps import AppConfig


class LaptopsAppsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'laptops_apps'
